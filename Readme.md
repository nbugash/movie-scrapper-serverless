# Movie Scrapper

## Description
This is a simple lambda application that interacts with `yts.mx`. It checks if there's a new 4K movie available.
This will NOT download the movie. This application will simply call the api from `yts.mx` and save a list in a mongo
database.

## Local development 
### Requirements
1. Serverless framework
2. Golang
3. Docker

### Setting up
1. Clone the repository
    ```git
    git clone git@github.com:nbugash/movie-scrapper-serverless.git
    cd movie-scrapper-serverless
    ```
2. Update the mongo.env with the correct information
3. Create a simple mongodb container
    ```bash
   export MONGO_ROOT_USERNAME=<root username>
   export MONGO_ROOT_PASSWORD=<root password>
   docker-compose -f infra/docker-compose.yaml up -d
   ```
4. Configure the serverless credentials
    ```bash
   serverless config credentials --provider aws --key [accesskey] --secret [secretkey] --profile [profile]  
   ```
5. Use the makefile provided to deploy the application
    ```bash
   export AWSPROFILE=<profile>
   make deploy 
   ```
