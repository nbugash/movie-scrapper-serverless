.PHONY: build clean deploy

build:
	env GOOS=linux go build -ldflags="-s -w" -o bin/scrapper scrapper/*.go

clean:
	rm -rf ./bin ./.serverless

dev_run: clean build
	sls invoke local -f scrapper

deploy: clean build
	sls --profile ${AWSPROFILE} deploy --verbose

teardown:
	sls --profile ${AWSPROFILE} remove