module gitlab.com/nbugash/movie-scrapper-serverless

go 1.14

require (
	github.com/aws/aws-lambda-go v1.16.0
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.5.0
	go.mongodb.org/mongo-driver v1.3.2
)
