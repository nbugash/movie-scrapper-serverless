package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"math"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/aws/aws-lambda-go/events"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Movie holds the information for each individual movie
type Movie struct {
	ID         int64    `json:"id" bson:"id"`
	URL        string   `json:"url" bson:"url"`
	Title      string   `json:"title" bson:"title"`
	Genres     []string `json:"genres" bson:"genre"`
	Summary    string   `json:"summary" bson:"summary"`
	Downloaded bool     `bson:"downloaded"`
}

// YtsData contains the metadata of the movies i.e
// total number of movies, number of movies retrieved, etc.
type YtsData struct {
	Limit      int     `json:"limit"`
	MovieCount int     `json:"movie_count"`
	PageNumber int     `json:"page_number"`
	Movies     []Movie `json:"movies"`
}

// YtsResponse first level json response from the server
type YtsResponse struct {
	Status        string  `json:"status"`
	StatusMessage string  `json:"status_message"`
	Data          YtsData `json:"data"`
}

type MovieScrapperConfig struct {
	ID         primitive.ObjectID `bson:"_id,omitempty"`
	Name       string             `json:"name" bson:"name,omitempty"`
	PageNumber float64            `json:"page_number" bson:"page_number,omitempty"`
}

type Response events.APIGatewayProxyResponse

var httpClient = &http.Client{}
var logger = logrus.New()

const (
	MongoUsername     string = "MongoUsername"
	MongoPassword     string = "MongoPassword"
	MongoHostname     string = "MongoHostname"
	MongoPort         string = "MongoPort"
	MongoEnv          string = "mongo.env"
	scheme            string = "https"
	ytsHost           string = "yts.mx"
	listMovieEndpoint string = "api/v2/list_movies.json"
	appDatabaseName   string = "movie_scrapper_db"
	configCollection  string = "config"
	movieCollection   string = "movies"
	mongoConnection   string = "mongodb://%s:%s@%s:%s/%s?retryWrites=false"
	quality           string = "2160p"
	initialPageNumber int    = 1
	rawQuery          string = "quality=%s&page=%d"
)

func main() {
	logger.SetLevel(logrus.InfoLevel)
	lambda.Start(Handler)
}

func run() {
	logger.Debug("Running run function")
	hasMore := true
	apiEndpoint := retrieveApiEndpointUrl()
	logger.Debug("Finish building api endpoint ", apiEndpoint.String())
	ytsResponse := &YtsResponse{}
	for {
		logger.Debug("Retrieving json response from yts endpoint", apiEndpoint.String())
		err := getJSON(apiEndpoint.String(), ytsResponse)
		logger.Debug("Response received!")
		if err != nil {
			os.Exit(0)
		}
		// save each movie id to a mongo database
		logger.Debug("Saving to mongodb")
		_, err = saveToDatabase(ytsResponse.Data.Movies, ytsResponse.Data.PageNumber)
		if err != nil {
			logger.Error("Saving failed")
			log.Fatal(err)
		}
		logger.Debug("Saving Success. Checking if there's more to retrieve")
		// check if there's more to retrieve
		hasMore = ytsResponse.Data.PageNumber*ytsResponse.Data.Limit < ytsResponse.Data.MovieCount
		if hasMore {
			logger.Debug("Retrieving more movies from the endpoint")
			apiEndpoint = &url.URL{
				Scheme:   scheme,
				Host:     ytsHost,
				Path:     listMovieEndpoint,
				RawQuery: fmt.Sprintf(rawQuery, quality, ytsResponse.Data.PageNumber+1),
			}
		} else {
			break
		}
	}
	logger.Debug("Saving ", ytsResponse.Data.PageNumber, " to the configuration collection")
	//save page number to config collection
	savePageNumberToConfig(ytsResponse.Data.PageNumber)
	logger.Debug("Nothing to retrieve anymore.")
	logger.Debug("Exiting scrapper")
}

func saveToDatabase(movies []Movie, page int) (bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 600*time.Second)
	defer cancel()
	if os.Getenv(MongoUsername) == "" || os.Getenv(MongoPassword) == "" || os.Getenv(MongoHostname) == "" || os.Getenv(MongoPort) == "" {
		_ = godotenv.Load(MongoEnv)
	}
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf(mongoConnection,
		os.Getenv(MongoUsername), os.Getenv(MongoPassword),
		os.Getenv(MongoHostname), os.Getenv(MongoPort), appDatabaseName)))
	if err != nil {
		return false, err
	}
	movieCollection := client.Database(appDatabaseName).Collection(movieCollection)
	numOfMoviesSaved := 0
	for _, movie := range movies {
		filterBy := bson.M{"Id": movie.ID}
		count, err := movieCollection.CountDocuments(ctx, filterBy)
		if err != nil {
			log.Fatalln(err)
		}
		if count == 0 {
			_, err = movieCollection.InsertOne(ctx, Movie{
				ID:         movie.ID,
				Title:      movie.Title,
				URL:        movie.URL,
				Genres:     movie.Genres,
				Summary:    movie.Summary,
				Downloaded: false,
			})
			if err != nil {
				log.Fatalln(err)
			}
			numOfMoviesSaved++
		}
	}
	logger.Debug(fmt.Sprintf("Saved %d movies to mongodb for page %d", numOfMoviesSaved, page))
	return true, nil
}

func savePageNumberToConfig(pageNumber int) {
	ctx, cancel := context.WithTimeout(context.Background(), 600*time.Second)
	defer cancel()
	if os.Getenv(MongoUsername) == "" || os.Getenv(MongoPassword) == "" || os.Getenv(MongoHostname) == "" || os.Getenv(MongoPort) == "" {
		_ = godotenv.Load(MongoEnv)
	}
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf(mongoConnection,
		os.Getenv(MongoUsername), os.Getenv(MongoPassword),
		os.Getenv(MongoHostname), os.Getenv(MongoPort), appDatabaseName)))
	if err != nil {
		log.Fatal(err)
	}

	configCollection := getConfigCollection(ctx, client.Database(appDatabaseName))
	filterBy := bson.M{
		"name": "config",
	}
	updateTo := bson.M{"$set": bson.M{"page_number": pageNumber}}
	var configuration MovieScrapperConfig
	err = configCollection.FindOneAndUpdate(ctx, filterBy, updateTo).Decode(&configuration)
	if err != nil {
		log.Fatal(err)
	}
}

func retrieveApiEndpointUrl() *url.URL {
	var configuration MovieScrapperConfig
	ctx, cancel := context.WithTimeout(context.Background(), 600*time.Second)
	defer cancel()
	if os.Getenv(MongoUsername) == "" || os.Getenv(MongoPassword) == "" || os.Getenv(MongoHostname) == "" || os.Getenv(MongoPort) == "" {
		_ = godotenv.Load(MongoEnv)
	}
	logger.Debug("Username: ", os.Getenv(MongoUsername))
	logger.Debug("Password: ", os.Getenv(MongoPassword))
	logger.Debug("Hostname: ", os.Getenv(MongoHostname))
	logger.Debug("Port: ", os.Getenv(MongoPort))
	logger.Debug("Database: ", appDatabaseName)
	logger.Debug(fmt.Sprintf(mongoConnection, os.Getenv(MongoUsername), os.Getenv(MongoPassword),
		os.Getenv(MongoHostname), os.Getenv(MongoPort), appDatabaseName))
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf(mongoConnection,
		os.Getenv(MongoUsername), os.Getenv(MongoPassword),
		os.Getenv(MongoHostname), os.Getenv(MongoPort), appDatabaseName)))
	if err != nil {
		log.Fatal(err)
	}
	configCollection := getConfigCollection(ctx, client.Database(appDatabaseName))
	filterBy := bson.M{
		"name": "config",
	}
	err = configCollection.FindOne(ctx, filterBy).Decode(&configuration)
	if err != nil {
		log.Fatal(err)
	}

	apiEndpoint := &url.URL{
		Scheme:   scheme,
		Host:     ytsHost,
		Path:     listMovieEndpoint,
		RawQuery: fmt.Sprintf(rawQuery, quality, int(math.Max(float64(initialPageNumber), configuration.PageNumber + 1))),
	}

	return apiEndpoint
}

func getConfigCollection(ctx context.Context, database *mongo.Database) *mongo.Collection {
	collections, err := database.ListCollectionNames(ctx, bson.D{})
	if err != nil {
		log.Fatal(err)
	}
	for _, value := range collections {
		if value == configCollection {
			return database.Collection(value)
		}
	}
	_, err = database.Collection(configCollection).InsertOne(ctx, MovieScrapperConfig{
		Name:       "config",
		PageNumber: 0,
	})
	if err != nil {
		log.Fatal(err)
	}
	return database.Collection(configCollection)
}

//getJSON retrieve the response body as a JSON object
func getJSON(url string, target interface{}) error {
	response, err := httpClient.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	return json.NewDecoder(response.Body).Decode(target)
}

func Handler(ctx context.Context) (Response, error) {
	run()
	var buf bytes.Buffer
	body, err := json.Marshal(map[string]interface{}{
		"message": "Okay so your other function also executed successfully!",
	})
	if err != nil {
		return Response{StatusCode: 404}, err
	}
	json.HTMLEscape(&buf, body)

	resp := Response{
		StatusCode:      200,
		IsBase64Encoded: false,
		Body:            buf.String(),
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}

	return resp, nil
}
